---
layout: handbook-page-toc
title: "Sales Prospecting"
description: "Prospecting is the process of initiating and developing new business by searching for potential customers, clients, or buyers for your products or services"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Prospecting is the process of initiating and developing new business by searching for potential customers, clients, or buyers for your products or services. The goal is to move these prospects through the flywheel until they convert to revenue-generating customers.

## Best Practices
The key to effective outbound prospecting is the delivery of relevant messages to the appropriate contacts at a suitable time.
1. Leverage partners!
    - Channel and alliance partners have relationships and know things that you don't 
1. Use combinations of account, engagement, and intent data (where available) to select and prioritize accounts, e.g.:
    - Account data
        - Target accounts that share characteristics of GitLab's [Ideal Customer Profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/)
        - Target accounts that have new leadership, show recent Merger & Acquisition activity, have contacts who are posting relevant content on social media, and more
        - Leverage GitLab advocates and champions when they transition to new target accounts 
    - Engagement data
        - Target prospects that have recently engaged with GitLab marketing activity and/or website content multiple times
    - Intent data
        - If available, target accounts that have demonstrated interest in a competitor and/or relevant industry trends
1. Lead with insights
    - Buyers are looking for trusted advisors who can address their challenges and help them achieve positive business outcomes. Share relevant, compelling, thought-provoking insights that teach prospects something they may not already know or that challenge their existing perspectives.
        - [Embrace Your Role as the Trusted Advisor](/handbook/sales/command-of-the-message/metrics/#embrace-your-role-as-the-trusted-advisor)
        - [Be a Good Storyteller](/handbook/sales/command-of-the-message/metrics/#be-a-good-storyteller)
1. Customize messages based on your knowledge of the account, the persona you are attempting to engage, and/or relevant insights
    - Test messaging and approaches and iterate until you find what works with different personas and other attributes (e.g. industry, geo, other)
    - Once you find approaches that work, create prewritten snippets to scale your prospecting efforts
1. Pursue a multithreading strategy (engage multiple user and buyer personas simultaneously) 
1. Engage via multiple channels (develop touch patterns for driving 12 to 24 touches over two to four weeks across three or more channels)
1. Contine to educate, nurture, and re-engage warm prospects

## Additional Resources
- [The 52 Best Sales Prospecting Tips](https://www.linkedin.com/learning/the-52-best-sales-prospecting-tips/what-s-in-this-course?u=2255073) (LinkedIn Learning course, 2h 37m)
- [Territory Planning](/handbook/sales/territory-planning/)
- [Keys to Successful Sales Pipeline Generation](https://gitlab.edcast.com/insights/keys) (e-learning, 30-45 minutes)
- [Social Selling Basics](/handbook/sales/training/social-selling/)